source('R/utils.R')
data <- readRDS('data/data.RDS')
train <- which(data$Time < as.POSIXct(strptime("2020-01-01 00:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))
test <- which(data$Year == 2020)

# we replace the outliers with the forecast 48 hours before
data$FTemperature[which(data$FTemperature < -8)] <- 
  data$FTemperature[which(data$FTemperature < -8) - 48]

# temperature
real <- data$Temperature
prev <- data$FTemperature
print(mean(abs((real-prev)[test])))
lag <- real[test-48+24*(data$Hour[test]<8)]
print(mean(abs(real[test] - lag)))
res <- real - prev
p <- 5
P <- 7
vars <- ar_variables(res, P*24, if (p==0) c(24*(2:P)) else c(24*(2:P), 48+1:p))
for (h in 0:7) {
  sel <- seq(1+h,nrow(vars),24)
  for (i in 1:(P-1))
    vars[sel,i] <- res[sel+P*24-24*i]
}
for (h in 0:23) {
  for (i in 1:p) 
    vars[seq(1+h,nrow(vars),24),P-1+i] <- res[seq(1,nrow(vars),24)+24*P-16-i]
}
vars <- cbind(prev[(P*24+1):nrow(data)], real[(P*24+1-48):(nrow(data)-48)], vars)
for (h in 0:7) {
  sel <- seq(1+h,nrow(vars),24)
  vars[sel,2] <- real[sel+24*P-24]
}
vars <- cbind(vars,1)
dim(vars)
d <- ncol(vars)

res_hat <- predict_offline_discriminate(vars, res[(P*24+1):nrow(data)],
                                        (P*24+1):length(train), 
                                        data$Hour[(P*24+1):nrow(data)])
data$FTemperature_corr1 <- prev + c(numeric(P*24), res_hat)
res_hat <- predict_online_discriminate(vars, res[(P*24+1):nrow(data)],
                                       data$Hour[(P*24+1):nrow(data)],
                                       theta1=matrix(0,d,1), P1=diag(d))
data$FTemperature_corr2 <- prev + c(numeric(P*24), res_hat)
print(mean(abs((real-data$FTemperature_corr1)[test])))

# CloudCover
real <- data$CloudCover
prev <- data$FCloudCover
print(mean(abs((real-prev)[test])))
lag <- real[test-48+24*(data$Hour[test]<8)]
print(mean(abs(real[test] - lag)))
res <- real - prev
p <- 1
P <- 15
vars <- ar_variables(res, P*24, if (p==0) c(24*(2:P)) else c(24*(2:P), 48+1:p))
for (h in 0:7) {
  sel <- seq(1+h,nrow(vars),24)
  for (i in 1:(P-1))
    vars[sel,i] <- res[sel+P*24-24*i]
}
if (p>0) {
  for (h in 0:23) {
    for (i in 1:p) 
      vars[seq(1+h,nrow(vars),24),P-1+i] <- res[seq(1,nrow(vars),24)+24*P-16-i]
  }
}
vars <- cbind(prev[(P*24+1):nrow(data)], real[(P*24+1-48):(nrow(data)-48)], vars)
for (h in 0:7) {
  sel <- seq(1+h,nrow(vars),24)
  vars[sel,2] <- real[sel+24*P-24]
}
for (h in 0:23)
  vars <- cbind(vars, 1 * (data$Hour[(P*24+1):nrow(data)] == h))
dim(vars)
d <- ncol(vars)

th <- ridge(vars[1:(length(train)-P*24),],res[(P*24+1):length(train)])
data$FCloudCover_corr1 <- prev + c(numeric(P*24), vars %*% th)
res_hat <- predict_online(list(theta=th, P=diag(d)), vars, res[(P*24+1):nrow(data)], delay=2)
data$FCloudCover_corr2 <- prev + c(numeric(P*24), res_hat)
print(mean(abs((real-data$FCloudCover_corr1)[test])))


# Pressure_kpa
real <- data$Pressure_kpa
prev <- data$FPressure_kpa
print(mean(abs((real-prev)[test])))
lag <- real[test-48+24*(data$Hour[test]<8)]
print(mean(abs(real[test] - lag)))
res <- real - prev
p <- 2
P <- 32
vars <- ar_variables(res, P*24, c(24*(2:P), 48+1:p))
vars <- cbind(prev[(P*24+1):nrow(data)], real[(P*24+1-48):(nrow(data)-48)], vars)
for (h in 0:23)
  vars <- cbind(vars, 1 * (data$Hour[(P*24+1):nrow(data)] == h))
d <- ncol(vars)
th <- ridge(vars[1:(length(train)-P*24),], res[(P*24+1):length(train)])
data$FPressure_kpa_corr1 <- prev + c(numeric(P*24), vars %*% th)
res_hat <- predict_online(list(theta=th, P=diag(d)), vars, res[(P*24+1):nrow(data)], delay=2)
data$FPressure_kpa_corr2 <- prev + c(numeric(P*24), res_hat)
print(mean(abs((real-data$FPressure_kpa_corr1)[test])))

# WindSpeed
real <- data$WindSpeed
prev <- data$FWindSpeed
print(mean(abs((real-prev)[test])))
lag <- real[test-48+24*(data$Hour[test]<8)]
print(mean(abs(real[test] - lag)))
res <- real - prev
p <- 3
P <- 22
vars <- ar_variables(res, P*24, if (p==0) c(24*(2:P)) else c(24*(2:P), 48+1:p))
for (h in 0:7) {
  sel <- seq(1+h,nrow(vars),24)
  for (i in 1:(P-1))
    vars[sel,i] <- res[sel+P*24-24*i]
}
if (p>0) {
  for (h in 0:23) {
    for (i in 1:p) 
      vars[seq(1+h,nrow(vars),24),P-1+i] <- res[seq(1,nrow(vars),24)+24*P-16-i]
  }
}
vars <- cbind(prev[(P*24+1):nrow(data)], real[(P*24+1-48):(nrow(data)-48)], vars)
for (h in 0:7) {
  sel <- seq(1+h,nrow(vars),24)
  vars[sel,2] <- real[sel+24*P-24]
}
for (h in 0:23)
  vars <- cbind(vars, 1 * (data$Hour[(P*24+1):nrow(data)] == h))
dim(vars)
d <- ncol(vars)
th <- ridge(vars[1:(length(train)-P*24),], res[(P*24+1):length(train)])
data$FWindSpeed_corr1 <- prev + c(numeric(P*24), vars %*% th)
res_hat <- predict_online(list(theta=th, P=diag(d)), vars, res[(P*24+1):nrow(data)], delay=2)
data$FWindSpeed_corr2 <- prev + c(numeric(P*24), res_hat)
print(mean(abs((real-data$FWindSpeed_corr1)[test])))

##### exponential smoothing on the forecasted temperature
# We assume that we have the forecast with a 48 hours horizon and that when we make 
# the forecast we have access to the realized temperature up to now
# Thus the exponential smoothing on day t is computed with 
# - the real temperature up to day t-2
# - the forecast on day t-1 and t
forecast_temps99 <- data$FTemperature_corr1
forecast_temps95 <- data$FTemperature_corr1
for (t in 2:24) {
  forecast_temps99[t] <- 0.99 * forecast_temps99[t-1] + 0.01 * data$FTemperature_corr1[t]
  forecast_temps95[t] <- 0.95 * forecast_temps95[t-1] + 0.05 * data$FTemperature_corr1[t]
}
for (day in 1:floor((nrow(data)-1)/24)) {
  s99 <- data$Temps99[(day-1)*24+8]
  s95 <- data$Temps95[(day-1)*24+8]
  for (t in ((day-1)*24+9):(day*24)) {
    s99 <- 0.99 * s99 + 0.01 * data$FTemperature_corr1[t]
    s95 <- 0.95 * s95 + 0.05 * data$FTemperature_corr1[t]
  }
  for (t in (day*24+1):min(day*24+24,nrow(data))) {
    s99 <- 0.99 * s99 + 0.01 * data$FTemperature_corr1[t]
    s95 <- 0.95 * s95 + 0.05 * data$FTemperature_corr1[t]
    forecast_temps99[t] <- s99
    forecast_temps95[t] <- s95
  }
}
data$FTemps99_corr1 <- forecast_temps99
data$FTemps95_corr1 <- forecast_temps95
forecast_temps99Max <- numeric(nrow(data))
forecast_temps99Min <- numeric(nrow(data))
for (t in 1:nrow(data)) {
  forecast_temps99Max[t] <- max(forecast_temps99[(((t-1)%/%24)*24+1):min(((t-1)%/%24)*24+24,nrow(data))])
  forecast_temps99Min[t] <- min(forecast_temps99[(((t-1)%/%24)*24+1):min(((t-1)%/%24)*24+24,nrow(data))])
}
data$FTemps99Max_corr1 <- forecast_temps99Max
data$FTemps99Min_corr1 <- forecast_temps99Min

saveRDS(data, 'data/data_corr.RDS')

