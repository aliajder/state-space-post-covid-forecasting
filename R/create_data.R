##### Putting the dataset in a big data frame
data <- data.frame(readxl::read_excel('data_raw/Actuals.xlsx'))
names(data) <- c('Time','Load','Pressure_kpa','CloudCover','Humidity','Temperature',
                 'WindDirection','WindSpeed')
cc <- data.frame(readxl::read_excel('data_raw/Forecasts/Cloudcover_percent.xlsx'))
data$FCloudCover <- cc[1:nrow(data),1]
press <- data.frame(readxl::read_excel('data_raw/Forecasts/Pressure_kpa.xlsx'))
data$FPressure_kpa <- press[1:nrow(data),1]
temp <- data.frame(readxl::read_excel('data_raw/Forecasts/Temperature_Celcius.xlsx'))
data$FTemperature <- temp[1:nrow(data),2]
wd <- data.frame(readxl::read_excel('data_raw/Forecasts/Winddirection_degree.xlsx'))
data$FWindDirection <- wd[1:nrow(data),1]
ws <- data.frame(readxl::read_excel('data_raw/Forecasts/Windspeed_kmh.xlsx'))
data$FWindSpeed <- ws[1:nrow(data),1]
data <- data[1:(nrow(data)-16),]

actuals_interim <- data.frame(read.csv('data_raw/Actuals_Interim.csv', header=T))
forecasts_interim <- data.frame(read.csv('data_raw/Forecasts_Interim.csv', header=T))[1:1704,c(3,2,1,4,5)]
# NA in the data
forecasts_interim[1009,] <- forecasts_interim[1010,]
data_interim <- cbind(actuals_interim, forecasts_interim)
#################################################################################################
# we need to be extra careful in handling dates
data_interim$Time <- as.POSIXct(data_interim$Time, tz='UTC')
names(data_interim) <- names(data)

#################################################################################################
actuals_test <- data.frame(read.csv('data_raw/Actuals_Jan17 8AM.csv', header=T))[1:32,]
for (d in 18:31)
  actuals_test <- rbind(actuals_test, data.frame(read.csv(paste('data_raw/Actuals_Jan ',d,' 8AM.csv', sep=''), header=T))[1:24,])
for (d in 1:9)
  actuals_test <- rbind(actuals_test, data.frame(read.csv(paste('data_raw/Actuals_Feb 0',d,' 8AM.csv', sep=''), header=T))[1:24,])
for (d in 10:16)
  actuals_test <- rbind(actuals_test, data.frame(read.csv(paste('data_raw/Actuals_Feb ',d,' 8AM.csv', sep=''), header=T))[1:24,])
actuals_test <- rbind(actuals_test, data.frame(read.csv('data_raw/Actuals_Feb 16.csv', header=T))[9:24,])
forecasts_test <- data.frame(read.csv('data_raw/Forecasts_Jan17 8AM.csv', header=T))[1:80,]
for (d in 18:31)
  forecasts_test <- rbind(forecasts_test, data.frame(read.csv(paste('data_raw/Forecasts_Jan ',d,' 8AM.csv', sep=''), header=T)[1:24,]))
for (d in 1:9)
  forecasts_test <- rbind(forecasts_test, data.frame(read.csv(paste('data_raw/Forecasts_Feb 0',d,' 8AM.csv', sep=''), header=T)[1:24,]))
for (d in 10:14)
  forecasts_test <- rbind(forecasts_test, data.frame(read.csv(paste('data_raw/Forecasts_Feb ',d,' 8AM.csv', sep=''), header=T)[1:24,]))
forecasts_test <- rbind(forecasts_test, data.frame(read.csv('data_raw/Forecasts_Feb 15 8AM.csv', header=T)[1:16,]))
data_test <- cbind(forecasts_test[,6], actuals_test[,2:8], forecasts_test[,c(3,2,1,4,5)])
data_test[,1] <- as.POSIXct(data_test[,1], tz='UTC')
names(data_test) <- names(data)

data <- rbind(data, data_interim, data_test)




##### Creating exponential smoothing variables of the temperature
# exponential smoothing on the realized temperature
real_temps99 <- data$Temperature
real_temps95 <- data$Temperature
for (t in 2:nrow(data)) {
  real_temps99[t] <- 0.99 * real_temps99[t-1] + 0.01 * data$Temperature[t]
  real_temps95[t] <- 0.95 * real_temps95[t-1] + 0.05 * data$Temperature[t]
}
data$Temps99 <- real_temps99
data$Temps95 <- real_temps95
real_temps99Max <- numeric(nrow(data))
real_temps99Min <- numeric(nrow(data))
for (t in 1:nrow(data)) {
  real_temps99Max[t] <- max(real_temps99[(((t-1)%/%24)*24+1):min(((t-1)%/%24)*24+24,nrow(data))])
  real_temps99Min[t] <- min(real_temps99[(((t-1)%/%24)*24+1):min(((t-1)%/%24)*24+24,nrow(data))])
}
data$Temps99Max <- real_temps99Max
data$Temps99Min <- real_temps99Min


##### exponential smoothing on the forecasted temperature
# We assume that we have the forecast with a 48 hours horizon and that when we make 
# the forecast we have access to the realized temperature up to now
# Thus the exponential smoothing on day t is computed with 
# - the real temperature up to day t-2
# - the forecast on day t-1 and t
forecast_temps99 <- data$FTemperature
forecast_temps95 <- data$FTemperature
for (t in 2:24) {
  forecast_temps99[t] <- 0.99 * forecast_temps99[t-1] + 0.01 * data$FTemperature[t]
  forecast_temps95[t] <- 0.95 * forecast_temps95[t-1] + 0.05 * data$FTemperature[t]
}
for (day in 1:floor((nrow(data)-1)/24)) {
  s99 <- real_temps99[(day-1)*24+8]
  s95 <- real_temps95[(day-1)*24+8]
  for (t in ((day-1)*24+9):(day*24)) {
    s99 <- 0.99 * s99 + 0.01 * data$FTemperature[t]
    s95 <- 0.95 * s95 + 0.05 * data$FTemperature[t]
  }
  for (t in (day*24+1):min(day*24+24,nrow(data))) {
    s99 <- 0.99 * s99 + 0.01 * data$FTemperature[t]
    s95 <- 0.95 * s95 + 0.05 * data$FTemperature[t]
    forecast_temps99[t] <- s99
    forecast_temps95[t] <- s95
  }
}
data$FTemps99 <- forecast_temps99
data$FTemps95 <- forecast_temps95
forecast_temps99Max <- numeric(nrow(data))
forecast_temps99Min <- numeric(nrow(data))
for (t in 1:nrow(data)) {
  forecast_temps99Max[t] <- max(forecast_temps99[(((t-1)%/%24)*24+1):min(((t-1)%/%24)*24+24,nrow(data))])
  forecast_temps99Min[t] <- min(forecast_temps99[(((t-1)%/%24)*24+1):min(((t-1)%/%24)*24+24,nrow(data))])
}
data$FTemps99Max <- forecast_temps99Max
data$FTemps99Min <- forecast_temps99Min


##### calendar data
data$Hour <- 0:(nrow(data)-1) %% 24
data$DayType <- lubridate::wday(data$Time)
data$Day <- lubridate::day(data$Time)
data$Month <- lubridate::month(data$Time)
data$Year <- lubridate::year(data$Time)
data$Toy <- lubridate::yday(data$Time) / 365
data$DateN <- as.numeric(floor((data$Time-data$Time[1])/(3600*24)))
# bank holidays or special days ?
data$BH <- 1 * (data$Month == 1 & data$Day == 12 | data$Month == 4 & data$Day == 17 |
                  data$Month == 8 & data$Day == 1 | data$Month == 9 & data$Day == 18 |
                  data$Month == 12 & data$Day == 11 | data$Month == 12 & data$Day == 18)


##### Lags
data$Load1D <- c(data$Load[1:24],data$Load[1:(nrow(data)-24)])
data$Load2D <- c(data$Load[1:24],data$Load[1:24],data$Load[1:(nrow(data)-48)])
data$Load1W <- c(data$Load[1:24],data$Load[1:24],data$Load[1:24],data$Load[1:24],
                 data$Load[1:24],data$Load[1:24],data$Load[1:24],
                 data$Load[1:(nrow(data)-7*24)])

##### Saving
saveRDS(data, 'data/data.RDS')

